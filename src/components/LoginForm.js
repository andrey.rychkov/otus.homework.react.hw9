import React, { useState } from 'react';
import axios from 'axios'

export default function LoginForm(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [token, setToken] = useState();
    
    function handleSubmit (e) {
        e.preventDefault();
        const url = '/api/login';
        axios.post(url, {
            email: email, password: password
        }).then((resp) => {
            console.log(resp.data);
               setToken(resp.data.token);
        })
        .catch(function (error) {
             console.error(error.response.data);
                alert('Not OK. Error: ' + JSON.stringify(error.response.data));
         });
    };
    
    function handleLogoff (e) {
        setToken(null);
    }

    const loginBlock = (
        <form onSubmit={handleSubmit}>
            <center>
                <div>
                    <label htmlFor='email'>Email</label>
                    <input type='email' id='email' value={props.email} onChange={(e) => setEmail(e.target.value)}  />
                </div>
                <div>
                    <label htmlFor='password'>Password</label>
                    <input type='password' id='password' value={props.password} onChange={(e) => setPassword(e.target.value)}/>
                </div>
                <button>Login</button>
            </center>
        </form>
     );

    const logoffBlock = (
        <form onSubmit={handleLogoff}>
            <center>
                <div>User {props.email} is successfully logged in</div>
                <button>Logoff</button>
            </center>
        </form>
    );
    
    return (<div>{token ? logoffBlock : loginBlock}</div>);
}
